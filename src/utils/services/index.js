import axios from "axios"

export const loadAllVideos = () => axios.get(`/video/video/mostViewedVideos`);
