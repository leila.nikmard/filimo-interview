import Search from "../../components/Search/Search";
import React, { useEffect, useState } from "react";

import "./style.scss";
import Chip from "../../components/Chip";

const Header = () => {

    const [hideSearch , setHideSearch] = useState(false)
    const chips = [
        {name:"فیلم"},
        {name: "سریال"},
        {name: "مجموعه ها"},
        {name: "سینمایی"}
    ];
    return(
        <header className="header">
            <div className="search">
                <Search />
            </div>
            <div className="chips">
                {chips.map((it,index) => <div key={index}><Chip name={it.name}/></div>)}
            </div>
        </header>
    )
};

export default Header;