import React from "react";
import accountIcon from "../../img/account.svg";
import homeIcon from "../../img/home.svg";
import searchIcon from "../../img/search.svg"

import "./style.scss"

const Navigation = () => {
    return(
        <div className="nav">
                <div className="nav__link">
                    <div className="nav__item">
                        <span>
                            <img src={homeIcon}/>
                        </span>
                        <span>خانه</span>
                    </div>
                </div>
                <div className="nav__link">
                    <div className="nav__item">
                        <span>
                            <img src={searchIcon}/>
                        </span>
                        <span>جستجو</span>
                    </div>
                </div>
                <div className="nav__link">
                    <div className="nav__item">
                        <span>
                            <img src={accountIcon} />
                        </span>
                        <span>حساب من</span>
                    </div>
                </div>
        </div>
    )

}

export default Navigation;