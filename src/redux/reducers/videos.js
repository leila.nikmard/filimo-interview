import {ActionTypes} from "../actionTypes";

const initialState = {
    videosList: [],
};

export const  videosReducer = (state = initialState , action) => {
    switch(action.type){
        case ActionTypes.LOAD_ALL_VIDEO:
            return {
                videosList: [...action?.data?.data]
            }
            default:
            return state
    }
}