import React from "react";
import VideoList from "../../components/Video/VideosList";

import "./style.scss"

const Home = () => {
    return (
        <div className="container">
            <VideoList />
        </div>
    )
}

export default Home;