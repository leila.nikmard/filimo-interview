import React from "react";
import Header from "./Layouts/Header";
import NavBar from "./Layouts/NavBar";
import RouterComponent from "./routes";
import Navigation from "./Layouts/navigation";

function App() {
  return (
    <>
      <NavBar />
      <Header />
      <RouterComponent />
      <Navigation />
    </>
  );
}

export default App;
