import React from "react";

import "./style.scss";

const Search = () => {
  return (
    <input
      className="search__box"
      placeholder="جست و جو..."
      type="text"
    />
  );
};

export default Search;
