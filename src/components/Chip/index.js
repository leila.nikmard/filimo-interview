import React from "react";

import "./style.scss";

const Chip = ({name}) => {
    return(
        <div className="chip">
            <span className="chip__name">{name}</span>
        </div>
    )
}

export default Chip