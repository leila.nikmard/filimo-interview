import React , {useRef,useEffect} from "react";

import "./style.scss";

const Video = ({data:{attributes}}) => {
    const videoRef = useRef(null);
    const videoContainerRef = useRef(null);

    const handleScroll = () => {
        const windowHeight = window.innerHeight / 2;
        const videoRefTop = videoContainerRef.current.getBoundingClientRect().top;
        const videoRefOffsetHeight = videoContainerRef.current.offsetHeight;

        if( videoRefTop > windowHeight - videoRefOffsetHeight  && videoRefTop < windowHeight){
            videoRef.current.play();
        }else{
            videoRef.current.pause();
        }
    }
    
    useEffect(() => {
        handleScroll();
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    },[]);

    

    return (
        <div ref={videoContainerRef} className="video__box">
            <video ref={videoRef} src={attributes?.preview_src} muted="muted"></video>
            <div className="video__description">{attributes?.description}</div>
        </div>
    )
}

export default Video;