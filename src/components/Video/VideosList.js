import React, { useEffect, useState } from "react";
import {useSelector , useDispatch} from "react-redux";
import { videosList } from "../../redux/actions/videos";
import {loadAllVideos} from "../../utils/services";
import Loading from "../Loading";
import Video from "./Video";

import "./style.scss";


const VideoList = () => {
    const dispatch = useDispatch();
    const [loading , setLoading] = useState(false);
    const videosListData = useSelector(state => state?.videos?.videosList);

    const loadVideos = async () => {
        setLoading(true);
       const response = await loadAllVideos();
       if(response.status === 200){
        setLoading(false);
       }
       dispatch(videosList(response?.data))
    }
    useEffect(() => {
        loadVideos()
    },[])
    return (
        <>
        {loading ? <div><Loading /></div> : videosListData?.map((it) => (
            <div key={it?.id}>
                <Video data={it}/>
            </div>
        ))}
        </>
    )
}

export default VideoList;