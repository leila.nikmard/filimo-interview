const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/video/video/mostViewedVideos',
    createProxyMiddleware({
      target: "http://api.aparat.com/fa/v1",
      secure: false,
      changeOrigin: true,
    })
  );
};