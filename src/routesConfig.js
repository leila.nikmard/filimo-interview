import Home from "./Pages/Home";

const routesConfig = [
    {
        path:"/",
        component: Home
    },
];

export default routesConfig;

